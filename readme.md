RECAP du 31 Janvier : 

Modèle MVC On a créé pleins de fichiers 
- Permet une meilleure organisation du code source. 
- Plus facile à maintenir 
- Utilisation de standard 
- Permet une meilleure séparation code source MYSQL/PHP/HTML ... 
  

- Model 
  - Correspond aux données de notre application. On retrouve les managers et la représentation objet de notre BDD. 
  - Les manager transformeront des requêtes mysql en objet (selection) / Des objets en requêtes SQL (insertion, édition ... )
    On retrouve la représentation objet de notre BDD (ici ma table User)
````injectablephp
<?php
class User {
    private $id;
    private $username;
    private $password;


    public function __construct($id, $username, $password){
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    public function getId(){
        return $this->id;
    }
    // ... 
}
````

La classe DbManager se connecte à notre BDD. Cest une classe abstraite on ne pourra pas l'instancier mais nos manager en hériteront

`````injectablephp
<?php
    abstract class DbManager {
        protected $bdd;

        public function __construct(){
            $this->bdd = new PDO("mysql:dbname=mvc;host=database","root","tiger");
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }
?>
````



Nos managers : Ils héritent de la classe DB Manager qui se charge d'effectuer la connexion avec notre BDD (héritage)

`````injectablephp
<?php
    class UserManager extends DbManager{

        public function getOneByUsername($username){
            $user = null;

            $query = $this->bdd->prepare("SELECT * FROM user WHERE username = :username");
            $query->execute([
                "username"=> $username
            ]);

            $result = $query->fetch();

            if($result){
                $user = new User($result["id"], $result["username"], $result["password"]);
            }

            return $user;
        }
    }
?>
`````

- Controller
  - Permettent le traitement de la requête HTTP. Il analyse la requête, effectue un traitement sur les données si nécessair affiche la vue en relation
  - traitement des formulaires / appels aux managers / retour de la vue 



`````injectablephp
class SecurityController {

    // Attribut que l'on utilisera afin de consulter la couche Model
    private $userManager;

    public function __construct(){
        // On initialise notre attribut qui sera un objet UserManager
        // On pourra ici appeler les mèthodes publiques de UserManager
        $this->userManager = new UserManager();
    }

    // Méthode qui traite les requêtes destinées à l'affichage ou ou traitement du formulaire de login
    public function login(){
        $errors = null;

        // Affichage du formulaire de login

        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // Effectuer les traitements sur mon formulaires
            $errors = $this->isValidLoginForm();

            if(count($errors) == 0){
                $user = $this->userManager->getOneByUsername($_POST["username"]);

                if(!is_null($user) && password_verify($_POST["password"], $user->getPassword())){
                    $_SESSION['user']= serialize($user);
                   header("Location: index.php?controller=default&action=homepage");
                } else {
                    $errors[] = "Identifiants incorrectes";
                }
            }

        }

        require "Vue/Security/login.php";
    }
}

`````
  
- Vue : 
  - Correspond à la partie visible par l'utilisateur. On fait simplement au PHP pour de l'affichage. 
  
`````injectablephp
<html>
<head>
    <?php
        include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>
<div class="container">
<h1>Bienvenue sur la page d'acceuil</h1>

<h1>Bonjour <?php echo($this->user->getUsername());?></h1>
<a href="index.php?controller=security&action=logout">Me déconnecter !</a>

</div>

</body>
</html>

`````


- Public : 
  - Dans ce dossier, on stock tout ce qui doit être accessible (js/css/images).

- Routeur : 
 - C'est le point d'entrée de notre application. Toutes les requêtes lui seront envoyés. Il renverra la requête vers le bon controller 

```injectablephp
<?php
// session_start();
require 'loader.php';

if(!isset($_GET["controller"]) && !isset($_GET["action"])){
    header("Location: index.php?controller=default&action=homepage");
}

if($_GET["controller"] == 'security'){
    $controller = new SecurityController();
    if($_GET["action"] == 'login'){
        $controller->login();
    } elseif ($_GET["action"] == 'register'){
        $controller->register();
    }
    if($_GET["action"] == 'logout'){
        $controller->logout();
    }
}
?>
```

ATTENTION Pour le moment si nous n'ajoutons pa le fichier loader.php qui contient toutes nos classes ça ne fonctionne pa 
Dans l'ordre : 

Notre DbManager
Nos class : Si héritage -> parent avant l'enfant
Nos managers
Nos controlleurs 

```injectablephp
<?php
require 'Model/Manager/DbManager.php';
require 'Model/User.php';
require "Model/Manager/UserManager.php";
require "Controller/AdminController.php";
require "Controller/DefaultController.php";
require "Controller/SecurityController.php";
?>
```
