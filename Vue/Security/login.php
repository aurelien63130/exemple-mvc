<html>
<head>
    <?php
    include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>
<div class="container">
<h1>Bienvenue sur la page d'acceuil</h1>

<form method="post" action="index.php?controller=security&action=login">
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Entrez un nom d'utilisateur">
    </div>
    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Mot de passe">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

</form>

    <?php
        if(!is_null($errors)){
            foreach ($errors as $error){
                echo '<div class="alert alert-danger" role="alert">
  '.$error.'
</div>';
        }
        }
    ?>


<a href="index.php?controller=security&action=regiter">M'enregistrer</a>
</div>
</body>
</html>