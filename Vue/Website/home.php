<html>
<head>
    <?php
        include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>
<div class="container">
    <?php
        include "Vue/Website/parts/menu.php"
    ?>
<h1>Homepage du site internet</h1>

    <div class="row">
    <?php
    foreach ($produits as $product){
        echo('<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="Public/uploads/'.$product->getImage().'" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">'.$product->getNom().'</h5>
    <p class="card-text">'.$product->getDescription().'</p>
    <form method="post" action="index.php?controller=website&action=formPanier&id='.$product->getId().'">
          <label for="quantity"> Combien ?</label><input type="number" id="quantity" name="quantity">
           <input type="submit" class="btn btn-primary">
    </form>
   
  </div>
</div>');
    }

    ?>
    </div>
</div>

</body>
</html>