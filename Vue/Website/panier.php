<html>
<head>
    <?php
    include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>
<div class="container">
    <?php
    include "Vue/Website/parts/menu.php"
    ?>

    <h1>Les produits du panier !</h1>
    <?php
        if(count($panier) == 0){
            echo('<h2 class="text-warning">Votre panier est vide</h2>');
        } else {
    ?>

            <h3>Vous prévoyez de dépenser <?php echo($prixTotal);?> euros</h3>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Article</th>
                <th scope="col">Image</th>
                <th scope="col">Quantité</th>
                <th scope="col">Prix unitaire</th>
                <th scope="col">Prix total</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
                foreach ($panier as $productOrder){
                    echo('<tr>
                <th>'.$productOrder->getProduit()->getNom().'</th>
                <td><img width="200" src="'.$productOrder->getProduit()->getImage().'"></td>

                <td>'.$productOrder->getQuantity().'</td>
                <th>'.$productOrder->getProduit()->getPrix().'</th>
                    <th>'. $productOrder->getQuantity() * $productOrder->getProduit()->getPrix().'</th>
                <td><a>Supprimer du panier !</a></td>
            </tr>');
                }
            ?>
            </tbody>
        </table>

    </div>
    <?php
        }
    ?>
</div>

</body>
</html>