<html>
<head>
    <?php
    // Le seul PHP VISIBLE dans ce document sont des fonctions PHP liées à l'affichage"
    // On réutilise ici les variabless / attributs de notre controller.
    include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>


<div class="container">
    <h1>Bonjour <?php echo($this->user->getUsername());?>
        <a href="index.php?controller=security&action=logout">Me déconnecter !</a></h1>

    <a href="index.php?controller=produit&action=add">Ajouter un produit !</a>

    <h2>Les produits en vente !</h2>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Description</th>
            <th scope="col">Image</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php
            foreach ($products as $product){
                echo('        <tr>
            <th scope="row">'.$product->getId().'</th>
            <td>'.$product->getNom().'</td>
            <td>'.$product->getDescription().'</td>
            <td><img class="img-array" src="Public/uploads/'.$product->getImage().'"></td>
            <td>
                <a href="index.php?controller=produit&action=one&id='.$product->getId().'">Voir en détail</a>
                <a href="index.php?controller=produit&action=edit&id='.$product->getId().'">Editer</a>    
                  <a href="index.php?controller=produit&action=delete&id='.$product->getId().'">Supprimer</a>    
            </td>
        </tr>');
            }
        ?>

        </tbody>
    </table>
</div>

</body>
</html>