<html>
<head>
    <?php
    // Le seul PHP VISIBLE dans ce document sont des fonctions PHP liées à l'affichage"
    // On réutilise ici les variabless / attributs de notre controller.
    include 'Vue/Parts/global-css.php'
    ?>
</head>
<body>


<div class="container">
    <h1>Bonjour <?php echo($this->user->getUsername());?>
        <a href="index.php?controller=security&action=logout">Me déconnecter !</a></h1>

    <a href="index.php?controller=produit&action=list">Retour !</a>

    <h2>Formulaire d'ajout de produit ! !</h2>

    <form method="post" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="nom" class="form-label">Nom du produit</label>
            <input type="text" name="nom" class="form-control" id="nom" >
        </div>

        <div class="mb-3">
            <label for="prix" class="form-label">Prix</label>
            <input type="text" name="prix" class="form-control" id="prix" >
        </div>

        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" id="description" >
        </div>

        <div class="mb-3">
            <label for="image" class="form-label">Image</label>
            <input type="file" name="image" class="form-control" id="image" />
        </div>

        <input class="btn btn-success" type="submit">

        <?php
            foreach ($errors as $error){
                echo('<div class="alert alert-danger" role="alert">
 '.$error.'
</div>');
            }
        ?>
    </form>
</div>

</body>
</html>