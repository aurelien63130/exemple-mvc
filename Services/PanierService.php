<?php
    class PanierService{

        public function getTotalPrice($panier){
            $price = 0;

            foreach ($panier as $productOrder){
                $price += $productOrder->getQuantity() * $productOrder->getProduit()->getPrix();
            }

            return $price;
        }
    }
?>