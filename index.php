<?php
// session_start();
require 'loader.php';

if(!isset($_GET["controller"]) && !isset($_GET["action"])){
    header("Location: index.php?controller=website&action=homepage");
}

if($_GET["controller"] == 'website'){
    $controller = new WebsiteController();
    if($_GET["action"] == 'homepage'){
        $controller->displayHomePage();
    }

    if($_GET["action"] == 'formPanier' && isset($_GET["id"])){
        $controller->panierForm($_GET["id"]);
    }

    if($_GET["action"]== "panier"){
        $controller->displayPanier();
    }
}

if($_GET["controller"] == 'security'){
    $controller = new SecurityController();
    if($_GET["action"] == 'login'){
        $controller->login();
    } elseif ($_GET["action"] == 'register'){
        $controller->register();
    }
    if($_GET["action"] == 'logout'){
        $controller->logout();
    }
}

if($_GET["controller"] == "default"){
    $controller = new DefaultController();
    if($_GET["action"]== "homepage"){
        $controller->homePage();
    }

}

// Nous ajoutons un controleur qui gérera les requêtes HTTP en relation avec nos produits
if($_GET["controller"] == "produit"){
    // SI jamais notre paramètre GET est égal à produit, nous créons un nouvel objet ProduitController
    $controller = new ProduitController();

    // Si notre action est égale à list,
    if($_GET["action"]== "list"){
        // On appel la méthode list du controlleur ProduitController
        $controller->list();
    }

    if($_GET["action"] == 'add'){
        $controller->add();
    }

    // Si notre action est égal à one ET que l'on a bien un paramètre get id
    if($_GET["action"]== "one" && isset($_GET["id"])){
        // On appel la méthode détail de notre controlleur
        $controller->detail($_GET["id"]);
    }

    if($_GET["action"] == "edit" && isset($_GET["id"])){
        $controller->edit($_GET["id"]);
    }

    if($_GET["action"] == 'delete' && isset($_GET["id"])){
        $controller->delete($_GET["id"]);
    }
}

if($_GET["controller"] == 'category' && $_GET["action"] == 'say-hello'){
    $controller = new CategoryController();
    $controller->sayHello();
}


if($_GET["controller"] == 'theme'){
    $controller = new ThemeController();
    if($_GET["action"] == 'change' && isset($_GET["value"])){
        $controller->changeTheme($_GET["value"]);
    }
}

