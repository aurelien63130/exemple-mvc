<?php
spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if(str_ends_with($file, 'Controller.php')){
        require 'Controller/'.$file;
    } else if(str_ends_with($file, 'Service.php')){
            require 'Services/'.$file;
    }
    else if(str_ends_with($file, 'Manager.php')){
        require 'Model/Manager/'.$file;
    } else if(str_ends_with($file, 'Service.php')){
        require 'Model/Service/'.$file;
    }
    else {
        require "Model/".$file;
    }
});
?>