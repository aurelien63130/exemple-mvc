<?php
class WebsiteController {

    private $productManager;
    private $sessionService;
    private $panierService;

    public function __construct()
    {
        $this->productManager = new ProductManager();
        $this->sessionService = new SessionService();
        $this->panierService = new PanierService();
    }

    public function displayHomePage(){
        // Selectionner de toutes les données relatives a la homepage

        $produits = $this->productManager->findAll();

        // Afficher ma vue

        require 'Vue/website/home.php';
    }

    public function displayPanier(){

        $panier = [];

        if(isset($this->sessionService->panier)){
            $panier = unserialize($this->sessionService->panier);
        }

        $prixTotal = $this->panierService->getTotalPrice($panier);

        require 'Vue/website/panier.php';
    }

    public function panierForm($id)
    {
        $errors = [];
        if(empty($_POST["quantity"])){
           $_POST["quantity"] = 1;
        }

        if(count($errors) == 0){
            $produitAjoute = $this->productManager->findOne($id);


            $productOrder = new ProductOrder($produitAjoute, $_POST["quantity"]);

            $panier = [];

            // Je regarde si j'ai déjà un panier dans ma session !
            if(isset($this->sessionService->panier)){
                $panier = unserialize($this->sessionService->panier);
            }

            $ajout = true;

            foreach ($panier as $productOrderBoucle){

                if($productOrderBoucle->getProduit()->getId() == $produitAjoute->getId()){
                    $productOrderBoucle->setQuantity($productOrderBoucle->getQuantity() + $_POST["quantity"]);
                    $ajout = false;
                }
            }

            // J'ajoute mon product order dans le panier
            if($ajout){
                $panier[] = $productOrder;
            }

            $this->sessionService->panier = serialize($panier);

            header("Location: index.php?controller=website&action=homepage");
        }

        require 'Vue/website/home.php';

    }
}