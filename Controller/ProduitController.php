<?php
class ProduitController extends AdminController {

    // On a ajouté un attribut ProductManager car nous en aurons besoin pour
    // interroger la couche Model
    private $productManager;

    // On oublie surtout pas d'appeler le constructeur parent Sinon la page ne sera pas soumise à authentification
    // Notre attribut protégé user sera nulle.
    public function __construct()
    {
        // On appel le constructeur parent
        parent::__construct();
        // On cré un nouvel objet ProductManager
        $this->productManager = new ProductManager();
    }

    // Dans notre mèthode list cf index.php
    // appelée avec l'url suivante : index.php?controller=produit&action=list
    public function list(){
        // On selectionne tons nos objets produits via notre manager
        $products = $this->productManager->findAll();
        // On affiche notre vue
        require "Vue/Produits/list.php";
    }


    // D'après le fichier routeur.php
    // Cette méthode est appelée avec l'URL suivant : index.php?controller=produit&action=one&id=votreId
    public function detail($id){
        // Notre manager retourne un objet Produit. Il aura l'id passé par le routeur (notre param get)
        $produit = $this->productManager->findOne($id);

        // On affiche notre vue de détail
        require "Vue/Produits/detail.php";

    }

    public function add(){
        $errors = [];
        // Effectuer un traitement losque le formulaire d'ajout est OK !
        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // Vérification des erreurs
            $errors = $this->validForm();

            // Valider l'image du produit

            if(!isset($_FILES["image"])){
                $errors[] = 'Veuillez ajouter une image sur le produit !';
            } else if(!in_array($_FILES["image"]["type"], ["image/jpeg", "image/png"])){
                $errors[] = "Le fichier n'est pas au bon format !";
            }

            else if ($_FILES["image"]["size"] > 2*(1048576) ){
                $errors[] = "Le fichier est trop lourd";
            }

            if(count($errors) == 0){

                // Upload notre image
                $extension = explode("/", $_FILES["image"]["type"])[1];
                $uniqFileName = uniqid().'.'.$extension;

                move_uploaded_file($_FILES["image"]["tmp_name"], "Public/uploads/".$uniqFileName);

                // On enregistre le lien de notre image dans notre BDD !

                $produit = new Product(null, $_POST["nom"],
                    $_POST["description"],
                    $uniqFileName, $_POST["prix"]);

                // Mon produit est dans le BDD
                $this->productManager->save($produit);

                // Redirection
                header("Location: index.php?controller=produit&action=list");

            }
            // En registrement de notre produit si on a pas d'erreur

        }

        require "Vue/Produits/form.php";
    }

    private function validForm(){
        $errors = [];
        if(empty($_POST["nom"])){
            $errors[] = 'Veuillez rentrer un nom';
        }

        if(empty($_POST["prix"])){
            $errors[] = 'Veuillez rentrer un prix';
        }

        return $errors;
    }

    public function edit($id)
    {
        $errors = [];


        // Je vais selectionner un produit en fonction de son ID !
        $produit = $this->productManager->findOne($id);



        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            $errors = $this->validForm();
            $image = null;


            if(isset($_FILES["image"]) && $_FILES["image"]["size"] != 0){

                if(!isset($_FILES["image"])){
                    $errors[] = 'Veuillez ajouter une image sur le produit !';
                } else if(!in_array($_FILES["image"]["type"], ["image/jpeg", "image/png"])){
                    $errors[] = "Le fichier n'est pas au bon format !";
                }

                else if ($_FILES["image"]["size"] > 2*(1048576) ){
                    $errors[] = "Le fichier est trop lourd";
                }


                if(count($errors) == 0){
                    $extension = explode("/", $_FILES["image"]["type"])[1];
                    $uniqFileName = uniqid().'.'.$extension;

                    move_uploaded_file($_FILES["image"]["tmp_name"], "Public/uploads/".$uniqFileName);

                    $produit->setImage($uniqFileName);
                }
            } else {
                $image = $produit->getImage();
            }

            if(count($errors) == 0){

                $produit->setNom($_POST["nom"]);
                $produit->setDescription($_POST["description"]);
                $produit->setPrix($_POST["prix"]);
                $this->productManager->update($produit);

                // Rediriger l'utiliateur
                header("Location: index.php?controller=produit&action=list");
            }
        }

        require "Vue/Produits/edit.php";

    }

    public function delete($id)
    {
        //J'appel mon manager pour supprimer mon objet
        $this->productManager->remove($id);
        // Je redirige l'utilisateur

        header("Location: index.php?controller=produit&action=list");
    }
}