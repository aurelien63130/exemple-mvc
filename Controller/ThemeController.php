<?php
class ThemeController{

    public function changeTheme($value)
    {
        setcookie("theme", $value, time() + 3600);

        header("Location: ". $_SERVER["HTTP_REFERER"]);
    }
}