<?php
class SecurityController {

    private $userManager;

    public function __construct(){
        $this->userManager = new UserManager();
    }

    public function login(){
        $errors = null;

        // Affichage du formulaire de login

        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // Effectuer les traitements sur mon formulaires
            $errors = $this->isValidLoginForm();

            if(count($errors) == 0){
                $user = $this->userManager->getOneByUsername($_POST["username"]);

                if(!is_null($user) && password_verify($_POST["password"], $user->getPassword())){
                    $_SESSION['user']= serialize($user);
                   header("Location: index.php?controller=default&action=homepage");
                } else {
                    $errors[] = "Identifiants incorrectes";
                }
            }

        }

        require "Vue/Security/login.php";
    }

    public function register(){

        // Affichage du formulaire pour s'enregistrer
        require "Vue/Security/register.php";
    }

    public function logout(){
        session_destroy();
        header("Location: index.php?controller=security&action=login");
    }

    private function isValidLoginForm(){
        $errors = [];

        if(empty($_POST["username"])){
            $errors[] = "Veuillez saisir un nom d'utilisateur";
        }

        if(empty($_POST["password"])){
            $errors[] = "Veuillez saisir un mot de passe";
        }

        return $errors;
    }

}