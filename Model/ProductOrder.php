<?php
class ProductOrder {
    private $produit;


    private $quantity;

    public function __construct(Product $product, $quantity){
        $this->produit = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduit(): Product
    {
        return $this->produit;
    }

    /**
     * @param Product $produit
     */
    public function setProduit(Product $produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

}