<?php
// On a créé notre ProductManager
// Il devra forcément être connecté à notre BDD
// On étend donc la classe DbManager.
class ProductManager extends DbManager {

    // Ici la méthode findAll qui permet de réccupérer tous les resultats de notre DB
    // Sous forme d'objet
    public function findAll(){
        $array = [];
        $query = $this->bdd->prepare("SELECT * FROM produit");
        $query->execute();
        // On réccupére ici tous nos resultats MySQL
        $results = $query->fetchAll();

        // On les re-transforme en objet  (Hydratation).
        foreach ($results as $result){
            $array[] = new Product($result["id"], $result["nom"], $result["description"], $result["image"], $result["prix"]);
        }

        return $array;
    }

    // Cette méthode fait une requête préparée de selection
    // Transforme notre résultat MySQL en objet (Hydratation)
    public function findOne($id)
    {
        $object = null;

        $query = $this->bdd->prepare("SELECT * FROM produit WHERE id = :id");
        $query->execute(["id"=> $id]);

        $result = $query->fetch();

        if($result){
            $object = new Product($result["id"], $result["nom"], $result["description"], $result["image"], $result["prix"]);
        }

        return $object;
    }

    public function save(Product $product){
        $query = $this->bdd->prepare("INSERT INTO produit (nom, description, image, prix)
VALUES (:nom, :description, :image, :prix)");

        $query->execute([
            "nom"=> $product->getNom(),
            "description"=> $product->getDescription(),
            "image"=> $product->getImage(),
            'prix'=> $product->getPrix()
        ]);
    }

    public function update(Product $product){
        $query = $this->bdd->prepare("UPDATE produit SET nom = :nom, description= :description, image = :image, prix = :prix WHERE id = :id");

        $query->execute([
            "nom"=> $product->getNom(),
            "description"=> $product->getDescription(),
            "image"=> $product->getImage(),
            'prix'=> $product->getPrix(),
            "id"=> $product->getId()
        ]);
    }

    public function remove($id)
    {
        $query = $this->bdd->prepare("DELETE FROM produit WHERE id = :id");
        $query->execute(["id"=> $id]);

    }
}