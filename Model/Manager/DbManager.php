<?php
    abstract class DbManager {
        protected $bdd;

        public function __construct(){
            $this->bdd = new PDO("mysql:dbname=mvc;host=database","root","tiger");
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }
?>